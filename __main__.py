import json

import boto3
import pulumi
from pulumi_aws import (apigatewayv2, dynamodb, get_caller_identity, iam,
                        lambda_)

resource_name_prefix = "recare"
config = pulumi.Config()
aws_config = pulumi.Config("aws")
org = config.require("org")
stack_name = pulumi.get_stack()
aws_region = aws_config.require("region")
image_repo_stack = pulumi.StackReference(
    f"{org}/{resource_name_prefix}-image-repos/prod"
)
aws_caller_identity = get_caller_identity()
ecr_client = boto3.client("ecr")

if stack_name == "dev":
    env = "dev"
else:
    env = "prod"


def get_ecr_repo_image_disgest(ecr_repo_name):
    response = ecr_client.describe_images(
        repositoryName=ecr_repo_name, imageIds=[{"imageTag": env}]
    )
    digest = response["imageDetails"][0]["imageDigest"]
    return digest


def generate_ecr_repo_url_with_digest(ecr_repo_url):
    return (
        f"""{ecr_repo_url}@{get_ecr_repo_image_disgest(ecr_repo_url.split("/")[-1])}"""
    )


dynamodb_table = dynamodb.Table(
    resource_name_prefix,
    attributes=[
        dynamodb.TableAttributeArgs(
            name="PK",
            type="S",
        ),
        dynamodb.TableAttributeArgs(
            name="SK",
            type="S",
        ),
    ],
    billing_mode="PROVISIONED",
    global_secondary_indexes=[
        dynamodb.TableGlobalSecondaryIndexArgs(
            name="InverseIndex",
            hash_key="SK",
            range_key="PK",
            projection_type="ALL",
            read_capacity=1,
            write_capacity=1,
        )
    ],
    hash_key="PK",
    range_key="SK",
    read_capacity=1,
    write_capacity=1,
    tags={"Environment": env},
)

lambda_iam_role = iam.Role(
    f"{resource_name_prefix}-lambda",
    assume_role_policy=json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "lambda",
                    "Action": "sts:AssumeRole",
                    "Principal": {"Service": "lambda.amazonaws.com"},
                    "Effect": "Allow",
                }
            ],
        }
    ),
    tags={"Environment": env},
)

lambda_iam_policy = pulumi.Output.all(ddb_table_arn=dynamodb_table.arn).apply(
    lambda kwargs: iam.Policy(
        f"{resource_name_prefix}-lambda",
        description="A test policy",
        policy=json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "Logging",
                        "Action": [
                            "logs:CreateLogGroup",
                            "logs:CreateLogStream",
                            "logs:PutLogEvents",
                        ],
                        "Effect": "Allow",
                        "Resource": f"arn:aws:logs:{aws_region}:{aws_caller_identity.account_id}:log-group:/aws/lambda/{resource_name_prefix}-*",
                    },
                    {
                        "Sid": "DDB",
                        "Action": "*",
                        "Effect": "Allow",
                        "Resource": [
                            kwargs["ddb_table_arn"],
                            f"""{kwargs["ddb_table_arn"]}/*""",
                        ],
                    },
                ],
            }
        ),
        tags={"Environment": env},
    )
)

lambda_role_policy_attachment = iam.RolePolicyAttachment(
    f"{resource_name_prefix}-lambda",
    role=lambda_iam_role.name,
    policy_arn=lambda_iam_policy.arn,
)

activity_lambda = lambda_.Function(
    f"{resource_name_prefix}-activity",
    role=lambda_iam_role.arn,
    package_type="Image",
    image_uri=image_repo_stack.get_output("activityLambdaEcrRepoUrl").apply(
        generate_ecr_repo_url_with_digest
    ),
    memory_size=128,
    timeout=10,
    environment=lambda_.FunctionEnvironmentArgs(
        variables={"DDB_TABLE_NAME": dynamodb_table.name}
    ),
    tags={"Environment": env},
)

appointment_lambda = lambda_.Function(
    f"{resource_name_prefix}-appointment",
    role=lambda_iam_role.arn,
    package_type="Image",
    image_uri=image_repo_stack.get_output("appointmentLambdaEcrRepoUrl").apply(
        generate_ecr_repo_url_with_digest
    ),
    memory_size=128,
    timeout=10,
    environment=lambda_.FunctionEnvironmentArgs(
        variables={"DDB_TABLE_NAME": dynamodb_table.name}
    ),
    tags={"Environment": env},
)

connect_lambda = lambda_.Function(
    f"{resource_name_prefix}-connect",
    role=lambda_iam_role.arn,
    package_type="Image",
    image_uri=image_repo_stack.get_output("connectLambdaEcrRepoUrl").apply(
        generate_ecr_repo_url_with_digest
    ),
    memory_size=128,
    timeout=10,
    environment=lambda_.FunctionEnvironmentArgs(
        variables={"DDB_TABLE_NAME": dynamodb_table.name}
    ),
    tags={"Environment": env},
)

health_record_lambda = lambda_.Function(
    f"{resource_name_prefix}-health-record",
    role=lambda_iam_role.arn,
    package_type="Image",
    image_uri=image_repo_stack.get_output("healthRecordLambdaEcrRepoUrl").apply(
        generate_ecr_repo_url_with_digest
    ),
    memory_size=128,
    timeout=10,
    environment=lambda_.FunctionEnvironmentArgs(
        variables={"DDB_TABLE_NAME": dynamodb_table.name}
    ),
    tags={"Environment": env},
)

user_lambda = lambda_.Function(
    f"{resource_name_prefix}-user",
    role=lambda_iam_role.arn,
    package_type="Image",
    image_uri=image_repo_stack.get_output("userLambdaEcrRepoUrl").apply(
        generate_ecr_repo_url_with_digest
    ),
    memory_size=128,
    timeout=10,
    environment=lambda_.FunctionEnvironmentArgs(variables={"USER_POOL_ID": " "}),
    tags={"Environment": env},
)

api_gw = apigatewayv2.Api(
    resource_name_prefix,
    protocol_type="HTTP",
    version="1.0.0",
    cors_configuration=apigatewayv2.ApiCorsConfigurationArgs(
        allow_headers=["*"], allow_methods=["*"], allow_origins=["*"]
    ),
    tags={"Environment": env},
)

api_gw_stage = apigatewayv2.Stage(
    resource_name_prefix,
    name="$default",
    api_id=api_gw.id,
    auto_deploy=True,
)

api_gw_activity_lambda_integration = apigatewayv2.Integration(
    f"{resource_name_prefix}-activity-lambda",
    api_id=api_gw.id,
    integration_type="AWS_PROXY",
    integration_uri=activity_lambda.invoke_arn,
    connection_type="INTERNET",
    integration_method="POST",
)

api_gw_appointment_lambda_integration = apigatewayv2.Integration(
    f"{resource_name_prefix}-appointment-lambda",
    api_id=api_gw.id,
    integration_type="AWS_PROXY",
    integration_uri=appointment_lambda.invoke_arn,
    connection_type="INTERNET",
    integration_method="POST",
)

api_gw_connect_lambda_integration = apigatewayv2.Integration(
    f"{resource_name_prefix}-connect-lambda",
    api_id=api_gw.id,
    integration_type="AWS_PROXY",
    integration_uri=connect_lambda.invoke_arn,
    connection_type="INTERNET",
    integration_method="POST",
)

api_gw_health_record_lambda_integration = apigatewayv2.Integration(
    f"{resource_name_prefix}-health-record-lambda",
    api_id=api_gw.id,
    integration_type="AWS_PROXY",
    integration_uri=health_record_lambda.invoke_arn,
    connection_type="INTERNET",
    integration_method="POST",
)

api_gw_user_lambda_integration = apigatewayv2.Integration(
    f"{resource_name_prefix}-user-lambda",
    api_id=api_gw.id,
    integration_type="AWS_PROXY",
    integration_uri=user_lambda.invoke_arn,
    connection_type="INTERNET",
    integration_method="POST",
)

api_gw_activity_lambda_permission = lambda_.Permission(
    f"{resource_name_prefix}-activity-lambda",
    action="lambda:InvokeFunction",
    function=activity_lambda.name,
    principal="apigateway.amazonaws.com",
    source_arn=api_gw.execution_arn.apply(lambda arn: f"{arn}/*/*/*"),
)

api_gw_appointment_lambda_permission = lambda_.Permission(
    f"{resource_name_prefix}-appointment-lambda",
    action="lambda:InvokeFunction",
    function=appointment_lambda.name,
    principal="apigateway.amazonaws.com",
    source_arn=api_gw.execution_arn.apply(lambda arn: f"{arn}/*/*/*"),
)

api_gw_connect_lambda_permission = lambda_.Permission(
    f"{resource_name_prefix}-connect-lambda",
    action="lambda:InvokeFunction",
    function=connect_lambda.name,
    principal="apigateway.amazonaws.com",
    source_arn=api_gw.execution_arn.apply(lambda arn: f"{arn}/*/*/*"),
)

api_gw_health_record_lambda_permission = lambda_.Permission(
    f"{resource_name_prefix}-health-record-lambda",
    action="lambda:InvokeFunction",
    function=health_record_lambda.name,
    principal="apigateway.amazonaws.com",
    source_arn=api_gw.execution_arn.apply(lambda arn: f"{arn}/*/*/*"),
)

api_gw_user_lambda_permission = lambda_.Permission(
    f"{resource_name_prefix}-user-lambda",
    action="lambda:InvokeFunction",
    function=user_lambda.name,
    principal="apigateway.amazonaws.com",
    source_arn=api_gw.execution_arn.apply(lambda arn: f"{arn}/*/*/*"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-activity-create",
    api_id=api_gw.id,
    route_key="POST /activity/{user_id}",
    target=api_gw_activity_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-activity-delete",
    api_id=api_gw.id,
    route_key="DELETE /activity/{user_id}/{activity_id}",
    target=api_gw_activity_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-activity-list",
    api_id=api_gw.id,
    route_key="GET /activity/{user_id}",
    target=api_gw_activity_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-appointment-create",
    api_id=api_gw.id,
    route_key="POST /appointment",
    target=api_gw_appointment_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-appointment-request-create",
    api_id=api_gw.id,
    route_key="POST /appointment/request/{user_id}",
    target=api_gw_appointment_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-appointment-request-list",
    api_id=api_gw.id,
    route_key="GET /appointment/request/{user_id}",
    target=api_gw_appointment_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-appointment-request-delete",
    api_id=api_gw.id,
    route_key="DELETE /appointment/request/{user_id}/{request_id}",
    target=api_gw_appointment_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-appointment-caregiver-list",
    api_id=api_gw.id,
    route_key="GET /appointment/caregiver/{caregiver_id}",
    target=api_gw_appointment_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-appointment-patient-list",
    api_id=api_gw.id,
    route_key="GET /appointment/patient/{patient_id}",
    target=api_gw_appointment_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-connect-request-create",
    api_id=api_gw.id,
    route_key="POST /connect/request/{user_id}",
    target=api_gw_connect_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-connect-request-delete",
    api_id=api_gw.id,
    route_key="DELETE /connect/request/{user_id}/{request_id}",
    target=api_gw_connect_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-connect-request-accept",
    api_id=api_gw.id,
    route_key="POST /connect/accept/{user_id}/{request_id}",
    target=api_gw_connect_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-connect-request-list",
    api_id=api_gw.id,
    route_key="GET /connect/request/{user_id}",
    target=api_gw_connect_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-health-record-create",
    api_id=api_gw.id,
    route_key="POST /health-record/{user_id}",
    target=api_gw_health_record_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-health-record-delete",
    api_id=api_gw.id,
    route_key="DELETE /health-record/{user_id}/{health_record_type}/{health_record_id}",
    target=api_gw_health_record_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-health-record-details",
    api_id=api_gw.id,
    route_key="GET /health-record/{user_id}",
    target=api_gw_health_record_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    ),
)

apigatewayv2.Route(
    f"{resource_name_prefix}-user-list",
    api_id=api_gw.id,
    route_key="GET /users",
    target=api_gw_user_lambda_integration.id.apply(lambda id: f"integrations/{id}"),
)

pulumi.export("dynamodbTableName", dynamodb_table.name)
pulumi.export("activityLambdaFunctionArn", activity_lambda.arn)
pulumi.export("appointmentLambdaFunctionArn", appointment_lambda.arn)
pulumi.export("connectLambdaFunctionArn", connect_lambda.arn)
pulumi.export("healthRecordLambdaFunctionArn", health_record_lambda.arn)
pulumi.export("userLambdaFunctionArn", health_record_lambda.arn)
pulumi.export("apiGwEndpoint", api_gw.api_endpoint)
